#! /bin/bash
# MERGE VIDEO SCRIPT
# this script searches the temp video file
# and merges the video and audio files into
# one flv in the finished video file.

# Location of ffmpeg binary
ffmpeg="/usr/local/bin/ffmpeg"

# Temporary videos location
tmp="tmp"

# Finished videoes location
vidoes="vid"

pwd
if [ "$(ls -A tmp)" ]; then
	for i in $tmp/*.wav; do
		filename=$(basename "$i")
		extension="${filename##*.}"
		filename="${filename%.*}"	
		echo $filename
		$ffmpeg -y -i $tmp/$filename.wav -i $tmp/$filename.webm $videos/$filename.flv
		rm $tmp/$filename.wav $tmp/$filename.webm
	done
else
	echo "no files to merge"
fi