function Interview(){
  var instance = this;
  this.prepTime;
  this.recordTime;
  this.currentRecordTime;
  this.waitSetInterval;
  this.recordSetInterval;
  this.question;
  this.companyId;
  this.studentId;
  this.connected = false;
  this.videoLocation = "test";

  this.setDefault = function(variable, defaultVar){
    variable = typeof variable !== 'undefined' ? variable : defaultVar;
    return variable;
  };

  this.setupInterview = function(waitTime,recordTime,question,companyId,studentId){
    
    // Set defaults for function
    waitTime = this.setDefault(waitTime,30);
    recordTime = this.setDefault(recordTime,120);
    question = this.setDefault(question,"Explain a difficult experience you had at work recently and how you overcame it");
    companyId = this.setDefault(companyId,0);
    studentId = this.setDefault(studentId,0);
      
    // Set variables to class 
    this.prepTime = waitTime;
    this.recordTime = this.currentRecordTime = recordTime;
    this.question = question;  
    this.companyId = companyId;
    this.studentId = studentId;

  };

  this.videoReady = function(loc){
    console.log("file is now ready!");
    var a = document.createElement('a');
    a.title = "here";
    a.href = loc;
    instance.updateOverheadText("You can preview your video <a target='_blank' href = \"" + a + "\"> here </a>");
    instance.videoLocation = loc;

    instance.upload();
  };

  this.createWebcam = function(){
    $(".webcam-question p").text(this.question);
    //create an instance of scriptcam on the passed modal
    $("#webcam").scriptcam({
      path:'video-interview/scriptcam/',
      fileReady:this.videoReady,
      onWebcamReady:this.wait,
      connected:function(){
        console.log("Connected to scriptcam servers...");
        instance.connected = true;
      },
      showDebug: true,
      fileName:'test',
      width:640,
      height:460,
      cornerRadius: 0
    });
  };

  
  this.wait = function(){
    console.log("starting to wait...");
    // set up timer to update prep time
    $(".webcam-overhead p").html("You have <span class=\"time\">"+instance.prepTime+" seconds</span> to prepare your answer");
    instance.waitSetInterval = setInterval(instance.updateWait,1000); 
  };

  this.updateWait = function(){
    // decrease preperation time, and update the header text
    instance.prepTime --;
    $(".webcam-overhead .time").html(instance.prepTime + " seconds");
    if(instance.prepTime <= 0){ // run when instance gets to 0
      clearInterval(instance.waitSetInterval); // remove timer
      instance.record(); // start recording
    }
  };

  this.upload = function(){
  $.ajax({
    type: "POST",
    url: 'video-interview/upload.php',
    data: { url: this.videoLocation, company: this.companyId, student: this.studentId },
    success: function (data) {
        console.log(data);
    }
});
  };
  
  this.record = function(){
    if (instance.connected === false){
      console.log("waiting for connection to scriptcam server");
      setTimeout(
        instance.record
        ,1000
      );
      return;
    }
    $.scriptcam.startRecording();
    instance.updateOverheadText("Recording...");
    instance.recordSetInterval = setInterval(instance.updateRecording,1000);
  };
  
  this.updateProgressBar = function(percent){
    percent = (percent) + "%";
    $('.webcam-overhead .progress').animate({width:percent});
  }

  this.updateRecording = function(){
    instance.currentRecordTime --;
    
    instance.updateProgressBar(100-((instance.currentRecordTime / instance.recordTime)*100)); //update progressbar to display how much time is left
    if (instance.currentRecordTime <= 0){ // when recording has run out of time
      clearInterval(instance.recordSetInterval); // stop timer
      instance.stopRecording(); // stop recording
    }
  };

  this.updateOverheadText = function(words){
    var selector = ".webcam-overhead p";
    $(selector).animate({opacity:0},function(){
      $(selector).html(words);
      $(selector).animate({opacity:1});
    });
  };

  this.stopRecording = function(){
    instance.updateProgressBar(0); // reset progress bar to 0
    $.scriptcam.closeCamera();
    instance.updateOverheadText("Converting video...");
  };
  
  this.createModal = function(callback){
    $("body").append("<div id='webcam-modal-container'></div>");
    $("#webcam-modal-container").load('video-interview/modal.html',function(){
      // stop users been able to close modal
      $('#webcamModal').modal({
        backdrop: 'static',
        keyboard: false,
        show:false
      });
      this.modaReady = true;
      if (callback !== undefined){
        callback();
      }
    });
  };

  this.showModal = function(){
    $("#webcamModal").modal('show');
  };
}
